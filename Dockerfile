FROM domecloud/alpine

MAINTAINER Dome C. <dome@tel.co.th>


RUN \
     apk update \
     && apk upgrade \ 
     && apk add --no-cache mariadb mariadb-client \
    && rm -f /var/cache/apk/*

COPY start.sh /
RUN chmod +x /start.sh
ENV TERM screen.xterm-new
ENV SHELL=/bin/bash

ENTRYPOINT ["/start.sh"]

